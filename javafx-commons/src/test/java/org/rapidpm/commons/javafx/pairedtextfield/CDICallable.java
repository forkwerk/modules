/*
 * Copyright [2013] [www.rapidpm.org / Sven Ruppert (sven.ruppert@rapidpm.org)]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.rapidpm.commons.javafx.pairedtextfield;

import java.util.concurrent.Callable;

import org.rapidpm.commons.cdi.se.CDIContainerSingleton;

/**
 * User: Sven Ruppert
 * Date: 08.10.13
 * Time: 15:40
 */
public abstract class CDICallable<T> implements Callable<T> {
    protected CDICallable() {
        CDIContainerSingleton.getInstance().activateCDI(this);
    }
}
