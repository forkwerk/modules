package org.rapidpm.commons.javafx.pairedtextfield.demologic.kotlin;

import org.rapidpm.commons.javafx.pairedtextfield.demologic.DemoLogic;

/**
 * User: Sven Ruppert
 * Date: 16.10.13
 * Time: 17:29
 */
public class KotlinDemoLogic implements DemoLogic {
    @Override
    public String doIt() {
        return "Something from kotlin";
    }
}
