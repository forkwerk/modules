/*
 * Copyright [2013] [www.rapidpm.org / Sven Ruppert (sven.ruppert@rapidpm.org)]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.rapidpm.commons.cdi.cache;

import java.util.Objects;

import org.rapidpm.lang.cache.generic.Cacheable;

/**
 * Created by Sven Ruppert on 31.07.13.
 */


@Cacheable(className = CachedClass.class, primaryKeyAttributeName = "txt")
public class CachedClass {

    private String txt;
    private Long value;

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof CachedClass)) return false;
//
//        CachedClass that = (CachedClass) o;
//
//        if (!txt.equals(that.txt)) return false;
//        if (!value.equals(that.value)) return false;
//
//        return true;
//    }
//
//    @Override
//    public int hashCode() {
//        int result = txt.hashCode();
//        result = 31 * result + value.hashCode();
//        return result;
//    }


    @Override public int hashCode() {
        return Objects.hash(txt, value);
    }

    @Override public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final CachedClass other = (CachedClass) obj;
        return Objects.equals(this.txt, other.txt) && Objects.equals(this.value, other.value);
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }


}
