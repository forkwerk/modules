package org.rapidpm.module.iot.tinkerforge.gui.fx.brickviewer;

public class Delta {
	double x, y;

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}