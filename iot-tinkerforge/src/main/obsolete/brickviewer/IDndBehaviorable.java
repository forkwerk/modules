package org.rapidpm.module.iot.tinkerforge.gui.fx.brickviewer;

/**
 * Created by Alexander Bischof on 21.02.14.
 */
public interface IDndBehaviorable {
    DnDBehavior getDnDBehavior();

    void setDnDBehavior(DnDBehavior anchorDnDBehavior);
}
