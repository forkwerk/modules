/*
 * Copyright [2013] [www.rapidpm.org / Sven Ruppert (sven.ruppert@rapidpm.org)]
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package org.rapidpm.module.se.commons.logger;

/**
 * User: Sven Ruppert Date: 18.09.13 Time: 10:03
 */
public class SolrAppender {
        /*
        extends AppenderSkeleton implements Serializable {

    private static final int QUEUE_SIZE = 100;
    private static final int THREAD_COUNT = 10;

    private ConcurrentUpdateSolrServer solrServer;
    private String localhostname;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
    private Random random = new Random(System.nanoTime());

    //    private String solrurl = "http://localhost:8983/solr/logger";
    private String solrurl = null;

    private void init() {

        solrServer = new ConcurrentUpdateSolrServer(solrurl, QUEUE_SIZE, THREAD_COUNT); //REFAC mandantenfähig machen
        solrServer.setParser(new XMLResponseParser());

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override public void run() {
                try {
                    if (liste.isEmpty()) {

                    } else {
                        solrServer.add(liste);
                        softCommit();
                        solrServer.commit();
                    }
                    solrServer.shutdownNow();
                } catch (SolrServerException | IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public SolrAppender() {
//        init();
    }

    public SolrAppender(boolean isActive) {
        super(isActive);
//        init();
    }


    @Override protected void append(LoggingEvent event) {
        if (solrServer == null) {
            init();
        } else {

        }

        subAppend(event);   //TODO change per CDI
    }


    @Override public void close() {
        try {
            solrServer.add(liste);
            softCommit();
            solrServer.commit();
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
    }


    @Override public boolean requiresLayout() {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }


    private int batchCounter = 0;
    private List<SolrInputDocument> liste = new ArrayList<>();

    protected void subAppend(LoggingEvent event) {
        int index = event.getLoggerName().lastIndexOf('.');
        String loggerName;

        if (index > -1) {
            loggerName = event.getLoggerName().substring(index + 1);
        } else {
            loggerName = event.getLoggerName();
        }

        final SolrInputDocument d = new SolrInputDocument();
        d.addField("loggername", loggerName);
        d.addField("hostname", localhostname);
        d.addField("id", System.nanoTime() + "-" + random.nextInt() + "-" + random.nextInt());

        d.addField("threadname", event.getThreadName());
        d.addField("timestamp", event.getTimeStamp());
        d.addField("date", sdf.format(new Date(event.getTimeStamp())));
        d.addField("level", event.getLevel());
        d.addField("message", event.getRenderedMessage());

        final LocationInfo locationInformation = event.getLocationInformation();
        final String className = locationInformation.getClassName();
        d.addField("className", className);
        final String fileName = locationInformation.getFileName();
        d.addField("fileName", fileName);
        final String lineNumber = locationInformation.getLineNumber();
        d.addField("lineNumber", lineNumber);
        final String methodName = locationInformation.getMethodName();
        d.addField("methodName", methodName);

        liste.add(d);

        try {
//            solrServer.commit();
            if (batchCounter < QUEUE_SIZE) {
                batchCounter = batchCounter + 1;
            } else {
                batchCounter = 0;
                solrServer.add(liste);
                softCommit();
                liste.clear();
            }
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }


    }


    private void softCommit() throws IOException, SolrServerException {
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.setParam("soft-commit", "true");
        updateRequest.setParam("content-type", "application/xml");
        updateRequest.setAction(UpdateRequest.ACTION.COMMIT, false, false);
        updateRequest.process(solrServer);
    }

    public String getSolrurl() {
        return solrurl;
    }

    public void setSolrurl(String solrurl) {
        this.solrurl = solrurl;
    }

    */
}
